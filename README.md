# MSc Course Introduction to snRNA-seq Analysis with R - 2024

## General Information

Dates: 08.04.2023 - 12.04.2023

Time:
* Mon-Fri: 10:00 - 17:00

Examination: Research Protocol

Deadline for Protocol: 13.05.2023 (midnight)

## Data set (for the course):
https://1drv.ms/f/s!AkxxMhqOM7HiuLJt36F52ue7WgXjEA?e=O1TIYT

## Data set (for the protocol):
https://1drv.ms/f/s!AkxxMhqOM7HiuLJVf6HYnc55Jvr5Gg?e=Bp4q0q

Data from [Woych et al., Science 2022, Cell-type profiling in salamanders identifies innovations in vertebrate forebrain evolution](https://www.science.org/doi/10.1126/science.abp9186)

## Code Notebooks

At the end of each day, we will upload notebooks with the code.

## Report

In your report you will follow the data analysis walk-through from the course, but with a different data set that we will make available here at the end of the course. Please follow the analyses of day 1, 2, 3, 4.1 and 5 of the course for the report.

Please annotate the clusters at resolution 0.3 (like you did on day 3 and the first part of day 4 of the course). When annotating, please justify why you chose each cell type for each cluster and name the genes on which you based your annotation. 

You do not need to subset the data set and annotate at a finer resolution (skip the detailed annotation as shown in part 4.2).

The report should include an introduction (around 300 words) describing the data set and descriptions/interpretations of the analyses and plots. The code notebooks that we provide also contain questions that you need to answer in your report.

You can send us the report as one or multiple knitted html files from the code notebooks (by email). If you're having trouble sending it, please let us know in time.